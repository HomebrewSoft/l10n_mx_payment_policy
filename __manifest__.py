{
    "name": "SAT Payment Methods for Mexican EDI",
    "version": "13.0.0.1.0",
    "author": "HomebrewSoft",
    "website": "https://homebrewsoft.dev",
    "license": "LGPL-3",
    "depends": [
        "l10n_mx_edi",
        "payment",
    ],
    "data": [
        # security
        "security/ir.model.access.csv",
        # data
        "data/sat_payment_method.xml",
        "data/server_actions.xml",
        "data/groups.xml",
        # reports
        # views
        "views/account_payment_term.xml",
        "views/account_move.xml",
    ],
}
