# -*- coding: utf-8 -*-
from odoo import _, models
from odoo.exceptions import UserError


class AccountPayment(models.Model):
    _inherit = 'account.payment'
    
    def action_register_payment(self):
        active_ids = self.env.context.get('active_ids')
        if not active_ids:
            return ''
        for move in self.env["account.move"].browse(active_ids):
            if not move.authorized_payment and move.type == "in_invoice":
                raise UserError(_("Payment is not authorized"))
        return super(AccountPayment, self).action_register_payment()