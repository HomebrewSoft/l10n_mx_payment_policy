# -*- coding: utf-8 -*-
from odoo import _, fields, models
from odoo.exceptions import UserError


class AccountMove(models.Model):
    _inherit = 'account.move'

    authorized_payment = fields.Boolean(
        string="Authorize Payment",
        copy=False,
    )
    
    def _l10n_mx_edi_get_payment_policy(self):
        _ = super(AccountMove, self)._l10n_mx_edi_get_payment_policy()
        
        return self.invoice_payment_term_id.sat_payment_method_id.code
    
    def action_authorize_payment(self):
        if self.env.user not in self.env.ref("l10n_mx_payment_policy.group_auth_payment_user").users:
            raise UserError(_("You have no rights to authorize payments!"))
        for move in self:
            move.authorized_payment = True
            move.message_post(
                body=_("Payment status: Authorized"),
            )
