# -*- coding: utf-8 -*-
from odoo import fields, models


class PaymentTerm(models.Model):
    _inherit = 'account.payment.term'

    sat_payment_method_id = fields.Many2one(
        comodel_name="sat.payment.method",
        string="Payment Method",
    )
