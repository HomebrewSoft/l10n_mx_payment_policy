# -*- coding: utf-8 -*-
from odoo import fields, models


class PaymentMethod(models.Model):
    _name = 'sat.payment.method'

    code = fields.Char()
    name = fields.Char()